# README #


### What is this repository for? ###

* A BDD implementation based on React Js/ Thymeleaf and
* 1.0.0-SNAPSHOT

### How do I get set up? ###

* Set up
  - Download the project from:
  - Use mvn + goal to clean, compile, test, install
  - Use mvnw spring-boot:run to run

* Configuration
  - Every dependant component will be embedded via pom configuration elements
  - Demo user: greg / turnquist. (from the master branch in order to log into the skeleton webapp)


* Aspects about the project structure worth to be noticed
  - The base structure of the project is present on the master branch
  - Other branches are derived from the master branch with the intent to become independant paths since each one correspond to the implementation relative to a particular bdd framework or tool
  - Therefore the current intent is not directed toward merging them back to the master (That may be considered later but not in this sprint [24])
  - When required, specific configuration details are provided on a branch itself, in addition to the general informations provided at the master level


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: Alain Lompo
* Other community or team contact
